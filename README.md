Script permettant de manager automatiquement la ventilation d'un serveur DELL disposant d'une carte iDrac.

Le controle de la ventilation est réalisée avec ipmitool en passant une valeur hecadécimale.

En analysant la vitesse des ventilateurs avec un pas de 0x01 j'obtient une augmentation de 120rpm.

On peut donc construite le tableau suivant :

    hexa	| rpm
    0x00	| 0
    0x01	| 120
    0x02	| 240
    0x0A	| 1200	#10%
    0x14	| 2400	#20%
    0x1E	| 3600	#30%
    0x28	| 4800	#40%
    0x32	| 6000	#50%
    0x3C	| 7200	#60%
    0x46	| 8400	#70%
    0x50	| 9600	#80%
    0x5A	| 10800	#90%
    Ox64	| 12000 #100%

Attention, il faut bien préciser à iDrac que nous souhaitons gérer les ventilateurs manuellement.
Il faut donc lancer une commande pour paramétrer ce paramètre, sinon, les ventilateurs retournerons à leurs vitesse d'origine.

Commande : 
ipmitool -I lanplus -H $idrac_ip -U $idrac_login -P $idrac_password raw 0x30 0x30 0x01 0x00
