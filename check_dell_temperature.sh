#!/bin/bash

temp_trigger_increase=30	#Augmente les ventilos quand la température dépasse cette valeur
temp_trigger_decrease=29	#Diminue les ventilos quand la température est en dessous de cette valeur

idrac_login=""
idrac_password=""
idrac_ip=""

#Récupération de la température système
temperature=$(ipmitool -I lanplus -H $idrac_ip -U $idrac_login -P $idrac_password sdr list | grep "Ambient Temp" | cut -d"|" -f2 | cut -d" " -f2 | grep -v "disabled")

#Récupération de la vitesse vitesse la plus haute de tous les ventilateurs
vitesse_ventilo=$(ipmitool -I lanplus -H $idrac_ip -U $idrac_login -P $idrac_password sdr list | grep "FAN" | grep "B" | cut -d"|" -f2 | cut -d" " -f2 | uniq | sort | tail -1)

#SI température système supérieure à la température max
if [[ $temperature -ge $temp_trigger_increase ]]
then
	nouvelle_vitesse=$((vitesse_ventilo+60))	#On ajoute 60rpm (sinon la conversion en hexa donne +2 par rapport au hexa d'origine
	nouvelle_vitesse_divisee=$((nouvelle_vitesse/120))	#On calcule le multiple de 120
	nouvelle_vitesse_hexa=$(printf "%x\n" $nouvelle_vitesse_divisee) #On récupère ce multiple pour le transformer en hexa

	#SI la nouvelle vitesse à appliquer est inférieure à la vitesse maximum des ventilos
	if [[ $nouvelle_vitesse -lt 12000 ]]
	then
		#INCREASE FAN
		#Appliquer la vitesse à la valeur $nouvelle_vitesse_hexa
		ipmitool -I lanplus -H $idrac_ip -U $idrac_login -P $idrac_password raw 0x30 0x30 0x02 0xff 0x$nouvelle_vitesse_hexa
	fi
#SI température inférieure à la température minimum
elif [[ $temperature -le $temp_trigger_decrease ]]
then
	nouvelle_vitesse=$((vitesse_ventilo-240))	#On retire 240
	nouvelle_vitesse_divisee=$((nouvelle_vitesse/120))	#On calcule le multiple de 120
	nouvelle_vitesse_hexa=$(printf "%x\n" $nouvelle_vitesse_divisee) #On convertie le nouveau multiple en hexa

	#SI la nouvelle vitesse est supérieure à 20% (2400rpm)
	if [[ $nouvelle_vitesse -gt 2400 ]]
	then
		#LOWER FAN
		#Appliquer la vitesse à la valeur $nouvelle_vitesse_hexa
		ipmitool -I lanplus -H $idrac_ip -U $idrac_login -P $idrac_password raw 0x30 0x30 0x02 0xff 0x$nouvelle_vitesse_hexa
	fi
fi

#Retour alerte Nagios
if [[ $temperature -le $temp_trigger_decrease ]] && [[ $temperature -gt 0 ]]
then
	echo "OK : Température : $temperature"
elif [[ $temperature -gt $temp_trigger_decrease ]] && [[ $temperature -lt $temp_trigger_increase ]]
then
	echo "OK : Température : $temperature"
elif [[ $temperature -ge $temp_trigger_increase ]] && [[ $temperature -le 35 ]]
then
	echo "WARNING : Température : $temperature"
elif [[ $temperature -gt 35 ]]
then
	echo "CRITICAL : Température : $temperature"
else
	echo "UNKNOWN : Température : $temperature" #Si jamais la température est hors des test (<0 ou autre valeure non numérique)
fi